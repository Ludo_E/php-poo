# Exercices PHP et POO

## 1 - Représentation de villes

### Création de la classe
Ecrire une classe "Ville" dans un fichier "Ville.class.php" implémentant le diagramme suivant :
![Diagramme UML de la classe Ville](./img-readme/ville.png)


La méthode **`__construct(string nom, string departement)`** est le [constructeur de la classe, il permet l'instanciation des objets de cette classe](https://www.php.net/manual/fr/language.oop5.decon.php). 

La méthode **`__toString()`** doit retourner une chaîne de caractère représentative d'un objet de la classe.

Dans le cas d'un objet de la classe `Ville` la méthode devra retourner une chaîne de caractère telle que :
`"La ville <nom-ville> est dans le département : <nom-du-département`

N'oubliez pas que vous pouvez utiliser `$this` au sein d'une méthode pour accéder aux attributs de l'objet manipulé.

### Ajout des getters en setters

Ajoutez les  [`getters` et `setters`](https://www.vincent-vanneste.fr/views/php/co/GetterSetter.html) de la classe `Ville`.

### Utilisation de la classe

Complétez les `TODOS` du fichier `ìndex_ville.php` pour tester votre classe.

### Documentation de la classe

Commentez votre classe en [utilisant la PHPDoc](https://grafikart.fr/tutoriels/phpdoc-1140).

---
## 2- Représentation de personnes

L'objectif est ici de simuler le fonctionnement du compte bancaire d'une ou plusieurs personnes.

### Création de la classe
Ecrire une classe "ComptePersonne" dans un fichier "ComptePersonne.class.php" implémentant le diagramme suivant :
![Uml de la classe ComptePersonne](./img-readme/compte-personne.png)

Voici quelques détails sur les méthodes :
- `__construct(nom, prenom, adresse, soldeInitial)` : le constructeur de la classe "ComptePersonne"
- `__toString()` : la représentation en chaîne de caractères d'un objet de la classe "ComptePersonne" (par exemple "`Michel Michel, 18 rue Gavroche : 500 euros`")
- `retrait(float montant): boolean` : effectue un retrait du montant passé en paramètre. Si le montant est supérieur à ce qu'il reste sur le compte le retrait ne se fait pas (pas de découvert autorisé) et la fonction doit renvoyer `false`. La fonction renvoie `true` si le retrait peut s'effectuer.
- `ajout(float montant): void` : effectue un ajout du montant passé en paramètre à l'objet. La fonction ne renvoie rien.
- `transferer(float montant, ComptePersonne cible): boolean` : effectue un transfère d'argent vers le compte `cible`. Si le compte source ne contient pas assez d'argent aucun tranfère n'est alors possible et la fonction renvoie `false`. Si le transfère peut s'effectuer la fonction renvoie `vrai`

### Ajout des getters en setters

Ajoutez les  [`getters` et `setters`](https://www.vincent-vanneste.fr/views/php/co/GetterSetter.html) de la classe `ComptePersonne`.

### Test de la classe
Utilisez le code fourni dans `index_compte.php` pour tester votre classe.

Le transfère d'argent se fait-il convenablement ?

### Documentation de la classe

Commentez votre classe en [utilisant la PHPDoc](https://grafikart.fr/tutoriels/phpdoc-1140).

---
## 3 - Représentation de salariés d'une entreprise

L’objectif est ici de concevoir un page web basée sur une approche objet et permettant de gérer des salariés d’une entreprise.

Fonctionnalités attendues :
- pouvoir créer des salariés d’une entreprise ;
- obtenir le salaire net à partir du salaire brut d’un salarié.

### Etape 1 : création de la classe salariée

Dans un premier temps, il vous faudra créer une classe nommée `Salarie` représentant un salarié d’une entreprise. Elle devra être contenue dans une fichier nommé `Salarie.class.php`.

Un salarié sera défini par les attributs suivants :
- **Matricule** : le matricule du salarié est son identifiant. La valeur du matricule permet donc de discriminer chaque objet de type salarié. Le matricule est une valeur composée de 7 caractères observant les règles suivantes nnXXXnn où n est un chiffre et X un caractère alphabétique (par exemple, le matricule « 11ABC22 » est correct, « Y5BN22 » est incorrect).
- **Nom** : le nom patronymique du salarié.
- **Prénom** : son prénom.
- **Salaire brut** : le salaire de référence, part salariale de charges sociales inclues.
- **Taux de charges sociales** : le taux de charges sociales (part salariale).
- **Date de naissance** : sa date de naissance.

Créez la classe `Salarie`.

Pour la gestion des dates vous pourrez utiliser des objets de la classe `DateTime` ([tutoriel disponible en cliquant ici](https://www.phpfacile.com/apprendre_le_php/dates_avec_classe_datetime)).

N'oubliez pas d'ajouter un **constructeur, les getters et les setters**.

### Etape 2 : ajout d'un système de vérification du matricule

La partie précédente présente un format de matricule attendu.

Pour un objet, ce matricule peut être modifié à deux moment :
- à la construction de l'objet (dans le constructeur) ;
- lors de l'appel au `setter` nommé `setMatricule`.

Dans l'une ou l'autre de ces fonctions il **est nécessaire de vérifier si le matricule passé en paramètre est correctement formaté**.

Ceci peut s'effectuer via l'utilisation d'un **expression régulière**.

Dans le cas où le matricule n'est pas correctement formaté vous allez pouvoir utliiser le système d'exception.

Une exception permet d'interrompre une fonction prématurément **lorsqu'un évènement inatendu et susceptible de provoquer des erreurs est rencontré**.

Une exception se lève avec le code suivant (cas d'une exception concernant notre vérification de matricule) :
```php
throw new Exception("Matricule incorrect");
```

Ainsi, à tout moment dans une fonction il est possible d'intégrer la ligne précédente pour **interrompre la fonction et renvoyer à la fonciton appelante un objet de la classe `Exception`**. C'est ce qu'on appelle "lever une exception".

Une exception levée est "rattrapable" dans la fonction appelante en utilisant un [bloc "try-catch"](https://www.phpfacile.com/apprendre_le_php/poo_les_exceptions).

Voici un exemple de code :
```php
try {
    // fonction susceptible de générer une exception
    une_fonction_qui_crash();

} catch (Exception $exception) {
    // ce bloc ne se déclenche que si une exception est levée
    // traitement de l'exception
    echo $exception->getMessage();
}
```

Dans ce dernier exemple on pourrait imaginer la fonction `une_fonction_qui_fait_tout_exploser()` ayant l'implémentation suivante :
```php
function une_fonction_qui_fait_tout_exploser() 
{
    echo "Bonjour, je suis une fonction et je vais crasher à la prochaine ligne";
    throw new Exception("Told you.");

    echo "Jamais vous ne verrez cette ligne vu que je crash à la ligne précédente.";
}
```

Pour une explication vidéo des exceptions [cliquez ici](https://grafikart.fr/tutoriels/exception-throw-try-catch-529).


L'idée est donc d'intégrer le fait de lever dans exceptions dans les fonction `__construct` et `setMatricule` pour interrompre le déroulé classique des fonctions et ainsi ne pas effectuer les traitements.