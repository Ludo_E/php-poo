<?php

class Salarie
{
    // TODO ajouter les autres attibuts

    private string $matricule;


    public function __construct(string $matricule)
    {
        // TODO insérer la vérification du matricule et jeter l'exception si le matricule ne convient pas
        throw new Exception("Matricule incorrect");
    }
    
    /**
     * Change le matricule
     *
     * @param string $matricule Le nouveau matricule
     */
    public function setMatricule(string $matricule): self
    {
        
        $this->matricule = $matricule;

        return $this;
    }

    /**
     * Vérifie si une chaine de caractère passée en paramètre est conforme au format d'un matricule.
     * 
     * @param string $stringATester La chaîne de caractères à tester
     * @return True si la chaîne est conforme, false sinon
     */
    public function checkMatricule(string $stringATester): bool
    {
        // TODO vérifier le format de la chaîne passée en paramètre

        return false;
    }
}