<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php

    require("./ComptePersonne.class.php");

    $compte1 = new ComptePersonne("Michel", "Michel", "18 rue Ducode", 1800);
    $compte2 = new ComptePersonne("Ladif", "Emma", "1 ter avenue de la Paille",0);

    $compte1->transferer(200, $compte2);
    echo "<p>{$compte1}</p>";
    echo "<p>{$compte2}</p>";

    ?>
</body>

</html>